
package com.multlock.key;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.text.DateFormat.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;



import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class Utils {
	
    public static final int KNOWN_DEVICES_SCAN_INTERVAL = 2500;
    
	//temporary ENUM the commands
    public static final byte ENUM_CMD_SEND_PUBLIC_KEY = 10;
    public static final byte ENUM_CMD_HANDSHAKE1 = 11;
    public static final byte ENUM_CMD_SET_OWNER = 12;
    public static final byte ENUM_CMD_SET_OWNER_RESPONSE = 13;
    public static final byte ENUM_CMD_KDF = 14;
    public static final byte ENUM_CMD_CREATE_NEW_KEY = 15;
    public static final byte ENUM_CMD_GET_NEW_KEY = 16;
    public static final byte ENUM_CMD_UNLOCK = 17;
    public static final byte ENUM_CMD_LOCK = 18;
    public static final byte ENUM_CMD_GET_NEW_KEY_RESPONSE = 19;
    public static final byte ENUM_CMD_KDF_RESPONSE = 20;
    public static final byte ENUM_CMD_UNLOCK_RESPONSE = 21;
    public static final byte ENUM_CMD_LOCK_RESPONSE = 22;
    public static final byte ENUM_CMD_CREATE_NEW_KEY_RESPONSE = 23;
    
    public static final byte ENUM_CMD_OP_SUCCESS_IMP = 100;
    public static final byte ENUM_CMD_OP_SUCCESS_EXP = 101;
    public static final byte ENUM_CMD_OP_ERROR = 102;
	
    public static final byte ENUM_CMD_GENERAL_ENCRYPTED = 120;
    public static final byte ENUM_CMD_GENERAL_PLAIN = 121;
	
	
	
    public static final String STR_KEY = "MTLT_KEY";
    public static final String STR_PUBLIC_KEY = "PUBLIC_KEY";
    public static final String STR_RANDOM_KEY = "RANDOM_KEY";
    public static final String STR_APP_ID = "APP_ID";
    public static final String STR_USER_ID = "USER_ID";
    public static final String STR_BLE_SIGNATURE = "BLE_SIGNATURE";
    public static final String STR_PREV_ADMIN_CODE = "PREV_ADMIN_CODE";
    public static final String STR_ADMIN_CODE = "ADMIN_CODE";
    public static final String STR_MODE = "MODE";
    public static final String STR_BLE_ASSOCIATION_ID = "BLE_ID";
    public static final String STR_BLE_EKEY = "LEKEY";
    public static final String STR_LOCK_NAME = "LOCK_NAME";
    public static final String STR_KEY_NAME = "KEY_NAME";
    public static final String STR_COMMAND = "COMMAND";
    public static final String STR_NEW_USER_PIN = "USER_PIN";
    public static final String STR_ERROR_CODE_TYPE = "ERROR_CODE";
    public static final String STR_ERROR_CODE_DETAIL = "ERROR_CODE";
    
    private static final String STR_LOCK_LIST = "LOCK_LIST";
    
    public static final int PUBLIC_KEY_LENGTH = 32;
    public static final int RANDOM_KEY_LENGTH = 32;
    public static final int APP_ID_LENGTH = 16;
    public static final int USER_ID_LENGTH = 16;
    public static final int BLE_SIGNATURE_LENGTH = 32;
    public static final int PREV_ADMIN_CODE_LENGTH = 6;
    public static final int ADMIN_CODE_LENGTH = 6;
    public static final int MODE_LENGTH = 1;
    public static final int BLE_ASSOCIATION_ID_LENGTH = 1;
    public static final int BLE_EKEY_LENGTH = 32;
    public static final int LOCK_NAME_LENGTH = 16;
    public static final int KEY_NAME_LENGTH = 16;
    public static final int COMMAND_LENGTH = 1;
    public static final int NEW_USER_PIN_LENGTH = 6;
    public static final int ERROR_CODE_TYPE_LENGTH = 1;
    public static final int ERROR_CODE_DETAIL_LENGTH = 2;
    
    private static final String TAG = "MultLock-Keys";
    private static final String LOCK_DELIMITER = "###";
    private static ArrayList<KeyData> lockList = null;
    
    private static class KeyData {
    	//private byte[] eKey = new byte[Utils.BLE_EKEY_LENGTH];
    	//private byte[] lockName = new byte[Utils.LOCK_NAME_LENGTH];
    	//private byte[] keyName = new byte[Utils.KEY_NAME_LENGTH];
    	private ByteArrayBuffer data = new ByteArrayBuffer(Utils.BLE_EKEY_LENGTH+Utils.LOCK_NAME_LENGTH+Utils.KEY_NAME_LENGTH);
    	
    	KeyData(byte[] eKey, byte[] lockName, byte[] keyName)
    	{
    		data.append(eKey, 0, eKey.length);
    		data.append(lockName, 0, lockName.length);
    		data.append(keyName, 0, keyName.length);
    	}
    	
    	private byte[] getKey()
    	{
    		return Arrays.copyOfRange(data.buffer(), 0, Utils.BLE_EKEY_LENGTH); 
    	}
    	
    	private byte[] getLockName()
    	{
    		return Arrays.copyOfRange(data.buffer(), Utils.BLE_EKEY_LENGTH, Utils.BLE_EKEY_LENGTH + Utils.LOCK_NAME_LENGTH); 
    	}
    	
    	private byte[] getKeyName()
    	{
    		return Arrays.copyOfRange(data.buffer(), Utils.BLE_EKEY_LENGTH + Utils.LOCK_NAME_LENGTH, Utils.BLE_EKEY_LENGTH+Utils.LOCK_NAME_LENGTH+Utils.KEY_NAME_LENGTH); 
    	}
    }
    
    private static byte[] getLocks(Context context)
    {
    	String locks = null;
    	
		try {
		       SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
		       locks = pref.getString(STR_LOCK_LIST, "");
		       byte[] array = Base64.decode(locks, Base64.DEFAULT);
		       return array;
		       //return locks.getBytes();
			} 	catch (Exception e) {
				Log.e("Utils", "getLocks: catch general exception");
				e.printStackTrace();
				return null;
			}
    }
    
    public static void setLocks(Context context, byte[] locks)
    {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		String str_locks = Base64.encodeToString(locks, Base64.DEFAULT);

		editor.putString(STR_LOCK_LIST, str_locks);
		
		// Commit the edits!
		editor.commit();
       return;
    }
    
    private static void initLockList(Context context)
    {
    	if (lockList != null)
    		return;
		lockList = new ArrayList<KeyData>();
    	byte[] locks = getLocks(context);
    	int len = locks.length;
    	int fullSize = Utils.BLE_EKEY_LENGTH + Utils.LOCK_NAME_LENGTH + Utils.KEY_NAME_LENGTH;
    	
    	if (len == 0 || len % fullSize != 0)
    		return;
    	
    	
    	for (int i=0; i< len / fullSize; i++)
    	{
    		byte[] eKey = Arrays.copyOfRange(locks, i * fullSize, i * fullSize + Utils.BLE_EKEY_LENGTH);
    		byte[] lockName = Arrays.copyOfRange(locks, i * fullSize + Utils.BLE_EKEY_LENGTH, i * fullSize + Utils.BLE_EKEY_LENGTH+Utils.LOCK_NAME_LENGTH);
    		byte[] keyName = Arrays.copyOfRange(locks, i * fullSize + Utils.BLE_EKEY_LENGTH+Utils.LOCK_NAME_LENGTH, i * fullSize + Utils.BLE_EKEY_LENGTH + Utils.LOCK_NAME_LENGTH + Utils.KEY_NAME_LENGTH);
    		KeyData keyData = new KeyData(eKey, lockName, keyName);
    		lockList.add(keyData);
    	}
    	return;
    }
    
    public static void addKey(Context context, byte[] lockName, byte[] keyName, byte[] eKey)
    {
    	Utils.initLockList(context);
    	KeyData keyData = new KeyData(eKey, lockName, keyName);
    	lockList.add(keyData);
    	//update persistent data
    	byte[] keys = Utils.getLocks(context);
    	String sKeys = new String(keys) + new String(keyData.data.buffer());
    	int l = sKeys.length();
    	Utils.setLocks(context, sKeys.getBytes());
    }
    
    public static void removeKey(Context context, String keyName)
    {
    	//done of the string or on array??
    	//what about keeping the key name and UUID?
    }
    
    public static void removeAllKeys(Context context)
    {
    	if (lockList != null)
    		lockList.clear();
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.remove(STR_LOCK_LIST);
		
		// Commit the edits!
		editor.commit();
       return;
    }
    
    public static boolean hasKeyForLock(Context context, String lock)
    {
    	Utils.initLockList(context);
    	for (int i=0; i<lockList.size(); i++)
    	{
    		KeyData keyData = lockList.get(i);
    		byte[] lockName = keyData.getLockName();
    		byte[] bLock = lock.getBytes();
    		if (Arrays.equals(lockName, bLock))
    			return true;
    	}
    	return false;	
    }
    
    public static byte[] getKeyForLock(Context context, String lock)
    {
    	Utils.initLockList(context);
    	for (int i=0; i<lockList.size(); i++)
    	{
    		if (lockList.get(i).getLockName().equals(lock.getBytes()))
    			return lockList.get(i).getKey();
    	}
    	return null;	
    }
    
    public static byte[] getAppId(Context context)
    {
    	String s=null;
    	//check if appId has been generated by random
    	
		try {
	       SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
	       String tmp = pref.getString(STR_APP_ID, null);
	       if (tmp != null) {
	    	   byte[] appId = Base64.decode(tmp, Base64.DEFAULT);
	    	   return appId;
	       }
	       //return locks.getBytes();
		} 	catch (Exception e) {
			Log.e("Utils", "getLocks: catch general exception");
			e.printStackTrace();
			return null;
		}
    	//return android.telephony.TelephonyManager.getDeviceId();
    	TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
    	s = telephonyManager.getDeviceId();
    	if (s == null) {
    		//generate an ID and store in memory
    		byte[] tmp = new byte[Utils.APP_ID_LENGTH];
    		new Random().nextBytes(tmp);
    		setAppId(context, tmp);
    		return tmp;
    	} else {
    		int len = s.length();
    		if (len > Utils.APP_ID_LENGTH)
    			return Arrays.copyOf(s.getBytes(), Utils.APP_ID_LENGTH);
    		else {
    			ByteArrayBuffer tmp = new ByteArrayBuffer(Utils.APP_ID_LENGTH);
    			int toFill = Utils.APP_ID_LENGTH;
    			while (toFill > 0) {
    				tmp.append(s.getBytes(), 0, (toFill > len ? len : toFill));
    				toFill -= len;
    			}
    			
    			return tmp.buffer();
    		}
    	}
    }
    
    private static void setAppId(Context context, byte[] appId)
    {
        SharedPreferences pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		String str_appId = Base64.encodeToString(appId, Base64.DEFAULT);

		editor.putString(STR_APP_ID, str_appId);
		
		// Commit the edits!
		editor.commit();
       return;
    }
    
 
}
