package com.multlock.key;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

//import org.spongycastle.asn1.nist.NISTObjectIdentifiers;
//import org.spongycastle.asn1.x9.X9ObjectIdentifiers;
//import org.spongycastle.crypto.generators.KDF2BytesGenerator;
//import org.spongycastle.crypto.generators.KDFCounterBytesGenerator;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SecPage extends Activity implements OnClickListener {

    private static String TAG = MainActivity.class.getSimpleName();

    private static String KPA_KEY = "kpA";
    private static String KPB_KEY = "kpB";
    private static byte[] aSecret = null;
    private static byte[] bSecret = null;
    private KeyPair g_kpA, g_kpB;
    

    private TextView curveNameText;
    private TextView fpSizeText;
    private TextView sharedKeyAText;
    private TextView sharedKeyBText;
    private TextView keyAText;
    private TextView keyBText;
    private SecretKey AES_key;

    private Button listAlgsButton;
    private Button generateKeysButton;
    private Button ecdhButton;
    private Button clearButton;

    private Crypto crypto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sec_page);

        setProgressBarIndeterminateVisibility(false);

        crypto = Crypto.getInstance();

        curveNameText = (TextView) findViewById(R.id.curve_name_text);
        fpSizeText = (TextView) findViewById(R.id.fp_size_text);
        sharedKeyAText = (TextView) findViewById(R.id.ska_text);
        sharedKeyBText = (TextView) findViewById(R.id.skb_text);
        keyAText = (TextView) findViewById(R.id.pka_text);
        keyBText = (TextView) findViewById(R.id.pkb_text);

        listAlgsButton = (Button) findViewById(R.id.list_algs_button);
        listAlgsButton.setOnClickListener(this);

        generateKeysButton = (Button) findViewById(R.id.generate_keys_button);
        generateKeysButton.setOnClickListener(this);

        ecdhButton = (Button) findViewById(R.id.ecdh_button);
        ecdhButton.setOnClickListener(this);

        clearButton = (Button) findViewById(R.id.clear_button);
        clearButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        if (v.getId() == R.id.list_algs_button) {
            Crypto.listAlgorithms("EC");
            Crypto.listCurves();
        } else if (v.getId() == R.id.generate_keys_button) {
            generateKeys(prefs);
        } else if (v.getId() == R.id.ecdh_button) {
            ecdh(prefs);
        } else if (v.getId() == R.id.clear_button) {
            clear(prefs);
        }
    }

    private void generateKeys(SharedPreferences prefs) {
        final SharedPreferences.Editor prefsEditor = prefs.edit();

        new AsyncTask<Void, Void, Boolean>() {

            ECParams ecp;
            Exception error;

            @Override
            protected void onPreExecute() {
                Toast.makeText(SecPage.this, "Generating ECDH keys...",
                        Toast.LENGTH_SHORT).show();

                setProgressBarIndeterminateVisibility(true);
            }

            @Override
            protected Boolean doInBackground(Void... arg0) {
                try {
                    ///////KeyPair kpA = crypto.generateKeyPairParams();
                    // saveToFile("kpA_public.der",
                    // kpA.getPublic().getEncoded());
                    // saveToFile("kpA_private.der",
                    // kpA.getPrivate().getEncoded());
                	KeyPair kpA = crypto.generateKeyPairNamedCurve();
                    KeyPair kpB = crypto.generateKeyPairNamedCurve();
                    ////////////KeyPair kpB = crypto.generateKeyPairParams();
                    g_kpA = kpA;
                    g_kpB = kpB;

                    saveKeyPair(prefsEditor, KPA_KEY, kpA);
                    saveKeyPair(prefsEditor, KPB_KEY, kpB);

                    return prefsEditor.commit();
                } catch (Exception e) {
                    Log.e(TAG, "Error doing ECDH: " + e.getMessage(), error);
                    error = e;

                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                setProgressBarIndeterminateVisibility(false);

                if (result) {
                    //curveNameText.setText("Curve name: " + ecp.name);
                    //fpSizeText.setText("Field size: "
                    //        + Integer.toString(ecp.getField().getFieldSize()));
                    
                    //add keys for comparison
                	int pri_len = g_kpA.getPrivate().getEncoded().length;
                	int pub_len = g_kpA.getPublic().getEncoded().length;
                    keyAText.setText(g_kpA.getPrivate().toString());
                    keyBText.setText(g_kpA.getPublic().toString());
                    //int pub_len = g_kpA.getPublic().toString().length();
                    //int pri_len = g_kpA.getPrivate().toString().length();
                    curveNameText.setText("public: " + Integer.toString(pub_len) + " private: " + Integer.toString(pri_len));

                    Toast.makeText(SecPage.this,
                            "Successfully generated and saved keys.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(
                    		SecPage.this,
                            error == null ? "Error saving keys" : error
                                    .getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        }.execute();
    }

    private void ecdh(final SharedPreferences prefs) {
        new AsyncTask<Void, Void, String[]>() {

            Exception error;

            @Override
            protected void onPreExecute() {
                Toast.makeText(SecPage.this,
                        "Calculating shared ECDH key...", Toast.LENGTH_SHORT)
                        .show();

                setProgressBarIndeterminateVisibility(true);
            }

            @Override
            protected String[] doInBackground(Void... arg0) {
                try {
                    KeyPair kpA = readKeyPair(prefs, KPA_KEY);
                    if (kpA == null) {
                        throw new IllegalArgumentException(
                                "Key A not found. Generate keys first.");
                    }
                    KeyPair kpB = readKeyPair(prefs, KPB_KEY);
                    if (kpB == null) {
                        throw new IllegalArgumentException(
                                "Key B not found. Generate keys first.");
                    }

                    aSecret = crypto.ecdh(kpA.getPrivate(),
                            kpB.getPublic());
                    bSecret = crypto.ecdh(kpB.getPrivate(),
                            kpA.getPublic());
                    
                    //KeyAgreement keyAgreement = KeyAgreement.getInstance(X9ObjectIdentifiers.dhSinglePass_stdDH_sha1kdf_scheme.getId(), "SC");
                    //KeyAgreement keyAgreement = (KeyAgreement)aSecret;
                    //AES_key = keyAgreement.generateSecret(NISTObjectIdentifiers.id_aes256_CBC.getId());
         
                    return new String[] { Crypto.hex(aSecret),
                            Crypto.hex(bSecret) };
                } catch (Exception e) {
                    Log.e(TAG, "Error doing ECDH: " + e.getMessage(), error);
                    error = e;

                    return null;
                }
            }

            @Override
            protected void onPostExecute(String[] result) {
                setProgressBarIndeterminateVisibility(false);

                if (result != null && error == null) {
                    sharedKeyAText.setText(result[0]);
                    sharedKeyBText.setText(result[1]);
                } else {
                    Toast.makeText(SecPage.this, error.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }

        }.execute();
    }
    
    public void onEncDec(View v) throws Exception
    {
    	String secA = sharedKeyAText.getText().toString();
    	String secB = sharedKeyBText.getText().toString();
    	
    	//byte[] aSecret = secA.getBytes();
    	//byte[] bSecret = secB.getBytes();
    	
        SecretKey secret1 = null;
        SecretKey secret2 = null;
        byte[] ciphertext = null;
        byte[] message = "Hello, World!".getBytes();
        byte[] clear = null;
        
		try {
			secret1 = Crypto.createAESKey(aSecret);
			secret2 = Crypto.createAESKey(bSecret);
			if (secret1.equals(secret2))
				Toast.makeText(this, "AES keys are equal", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(this, "AES keys are NOT equal", Toast.LENGTH_SHORT).show();
			ciphertext = Crypto.encrypt(secret1, message, null);
			clear = Crypto.decrypt(secret1, ciphertext);
			
	        String s = new  String(clear);//clear.toString();
	        
	        keyAText.setText(new String(message));
	        keyBText.setText(s);
	        
	    	return;
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidParameterSpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //KDF2BytesGenerator k;
    }

    private void clear(SharedPreferences prefs) {
        curveNameText.setText("");
        fpSizeText.setText("");
        sharedKeyAText.setText("");
        sharedKeyBText.setText("");

        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(KPA_KEY + "_private", null);
        prefsEditor.putString(KPA_KEY + "_public", null);
        prefsEditor.putString(KPB_KEY + "_private", null);
        prefsEditor.putString(KPB_KEY + "_public", null);

        prefsEditor.commit();

        Toast.makeText(SecPage.this, "Deleted keys.", Toast.LENGTH_LONG)
                .show();
    }
    
    byte[] Signature;
    final String source = "This is my signature";
    
    public void onSign(View v)
    {
    	try {
			Signature = crypto.sign(v.getContext(), source);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return;
    }
    
    public void onVerify(View v)
    {
    	try {
			boolean b = crypto.verify(v.getContext(), null, source, Signature);
			if (b)
				Toast.makeText(v.getContext(), "Verification SUCCEEDED", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(v.getContext(), "Verification FAILED", Toast.LENGTH_SHORT).show();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return;
    }

    private void saveKeyPair(SharedPreferences.Editor prefsEditor, String key,
            KeyPair kp) {
        String pubStr = Crypto.base64Encode(kp.getPublic().getEncoded());
        String privStr = Crypto.base64Encode(kp.getPrivate().getEncoded());

        prefsEditor.putString(key + "_public", pubStr);
        prefsEditor.putString(key + "_private", privStr);
    }

    @SuppressWarnings("unused")
    private void saveToFile(String filename, byte[] bytes) throws Exception {
        File file = new File(Environment.getExternalStorageDirectory(),
                filename);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes);
        fos.flush();
        fos.close();
    }

    private KeyPair readKeyPair(SharedPreferences prefs, String key)
            throws Exception {
        String pubKeyStr = prefs.getString(key + "_public", null);
        String privKeyStr = prefs.getString(key + "_private", null);

        if (pubKeyStr == null || privKeyStr == null) {
            return null;
        }

        return crypto.readKeyPair(pubKeyStr, privKeyStr);
    }
}

