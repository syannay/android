package com.multlock.key;

import java.util.UUID;

import android.os.Bundle;

public interface BleInterface {
	
	//sent on each BLE device found by scan
	public void onLeScanDeviceFound();
	
	//called when scan stopped
	public void onStopScan();
	
	//called on response to public key exchange between lock and mobile
	public void onSendPublicKeyResponse(Bundle response);

	//called when handshake done
	public void onHandshakeResponse(Bundle response);
	
	//called in response to setOwner
	public void onSetOwnerResponse(Bundle response);
	
	//called when last request succeeded (implicit call)
	public void onLastRequestSuccess();
	
	//called when command succeeded - command defined in response explicitly
	public void onRequestSuccess(Bundle response);
	
	//called when command failed - command defined in response explicitly
	public void onRequestError(Bundle response);

	//called when GATT connected
	public void onGattConnected();

	//called when GATT disconnected
	public void onGattDisconnected();

	//called when failed to find requested services in connected device
	public void onDiscoverServiceFailed();

	//called when we get a response to control service read
	public void onResponseControlRead();

	//called after we read primary characteristic value
	public void onResponsePrimaryRead();

	//called on GATT error
	public void onGattError();

	//called on request control write action to lock
	public void onRequestControlWrite();

	//called on notification on Control characteristic change
	public void onResponseControlChanged();

	//called on notification on Primary payload characteristic change
	public void onResponsePrimaryChanged();

	//called on notification on Secondary payload characteristic change
	public void onResponseSecondaryChanged();
	
	//called on error sending data to lock
	public void onSendError();
	
	//called in response to GetKey
	public void onGetNewKeyResponse(Bundle response);
	
	//called in response to KDF
	public void onKdfResponse(Bundle response);
	
	//called when a new device found on scan to filter illegal devices
	public boolean isDeviceApproved(String name, UUID uuid);
}
