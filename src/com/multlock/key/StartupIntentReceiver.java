package com.multlock.key;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Debug;

public class StartupIntentReceiver extends BroadcastReceiver
{
@Override
	public void onReceive(Context context, Intent intent) {
		Intent serviceIntent = new Intent();
		serviceIntent.setAction(".BluetoothLeService");
		context.startService(serviceIntent);
	}
}
