package com.multlock.key;

import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.*;


public class MainActivity extends Activity implements BleInterface{
	
    private byte runningSeq = 0;
	
    private static final String TAG = "MTLT";
	
    private BleUtils ble;
    
    private Handler mHandler = null;
    
    private byte[] lastRes;
    
    private byte[] eUserKey;
    
    Context context;
    
    TextView DeviceConn = null;
    
    EditText tvOutput = null;
    
    //parameters to support communication test
    byte[] BlePublicKey = new byte[Utils.PUBLIC_KEY_LENGTH];

	byte[] lastRandom = "this is the random num of mobile".getBytes();
    byte[] appId = "application id 12".getBytes();
    byte[] userId = "lock admin 12345".getBytes();
    byte[] newUserId = "lock user 123456".getBytes();
    byte[] prevCode = "000000".getBytes();
    byte[] newCode = "000000".getBytes();
	byte[] newUserPIN = "usrpin".getBytes();
	byte[] eKey = "this is a digital key of mobile1".getBytes();
	byte[] lockName = "Mul-T-Lock 007-1".getBytes();
	byte[] ownerKeyName = "Owner Key 123456".getBytes();
	byte[] userKeyName = "User Key 1234567".getBytes();
	byte handShakeId = 0;
    


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setProgressBarIndeterminate(true);
		ble = new BleUtils();
		context = this;
		DeviceConn = (TextView)findViewById(R.id.textView1);
		tvOutput = (EditText)findViewById(R.id.editText1);
		
	    mHandler = new Handler() {
	        @Override
	        public void handleMessage(Message msg) {

	        	switch (msg.what) {
	        	case 0:
	            	Toast.makeText(context, "Response Char changed", Toast.LENGTH_SHORT).show();
	        		break;
	        	case 1:
	        		break;
	        	case 2:
	        		Toast.makeText(context, "Discovering Services...", Toast.LENGTH_SHORT).show();
	        		break;
	        	case 3:
	        		Toast.makeText(context, "Device Disconnected!!", Toast.LENGTH_SHORT).show();
	        		DeviceConn.setText("Disconnected...");
	        		break;
	        	case 4:
	        		//long elapsed = time_after - time_before;
	        		//String s_elapsed = Long.toString(elapsed);
	        		//Toast.makeText(context, "Write callback to Command Char - time: " + s_elapsed + " milli sec", Toast.LENGTH_SHORT).show();
	        		//Log.d(TAG, "Write time elapsed: " + s_elapsed + " milli sec");
	        		break;
	        	case 5:
	        		Toast.makeText(context, "Write to Command Char FAILED!!", Toast.LENGTH_SHORT).show();
	        		break;
	        	case 6:
	        		Toast.makeText(context, "Request Succeeded", Toast.LENGTH_SHORT).show();
	        		break;
	        	case 7:
	        		Toast.makeText(context, "Request Failed", Toast.LENGTH_SHORT).show();
	        		break;
	        	case 8:
	        		Toast.makeText(context, "Services not found!!", Toast.LENGTH_SHORT).show();
	        		DeviceConn.setText("Did not discover critical services!!!");
	        		break;
	        	case 9:
	        		Toast.makeText(context, "Checksum Error in Response!!", Toast.LENGTH_SHORT).show();
	        		break;
	    		case Utils.ENUM_CMD_UNLOCK_RESPONSE:
	        		Toast.makeText(context, "Door unlocked successfully", Toast.LENGTH_SHORT).show();
	        		break;
	    		case Utils.ENUM_CMD_LOCK_RESPONSE:
	        		Toast.makeText(context, "Door locked successfully", Toast.LENGTH_SHORT).show();
	        		break;
	    		case Utils.ENUM_CMD_CREATE_NEW_KEY_RESPONSE:
	        		Toast.makeText(context, "New Key Created successfully", Toast.LENGTH_SHORT).show();
	        		break;
	        	case 100:
	        		Bundle b = msg.getData();
	        		String s = b.getString("HANDLER_MESSAGE");
	        		//Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
	        		tvOutput.setText(s);
	        	}
	        	
	            return;
	        }
	    };
	    
	    //ble.init(this, mHandler, this);

	    
	}

	
    @Override
    protected void onStart() {
        super.onStart();
        ble.init(this, mHandler, this);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        //ble.init(this, mHandler, this);
    }
    
    
    @Override
    protected void onPause() {
        super.onPause();
        //Cancel any scans in progress
        //disconnectBLE();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Cancel any scans in progress
        //disconnectBLE();
    }
    
    private void disconnectBLE()
    {
        ble.disconnect();
        ble.clearDevices();
    }

    
    @Override
    protected void onStop() {
        super.onStop();
        disconnectBLE();
    }
 

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	super.onPrepareOptionsMenu(menu);
        // Add the "scan" option to the menu
        //getMenuInflater().inflate(R.menu.settings, menu);
    	/*
        //Add any device elements we've discovered to the overflow menu
        for (int i=0; i < ble.getDeviceSize(); i++) {
        	int id = ble.getKey(i);
        	String name = ble.getDeviceName(i);
            menu.add(0, id, 0, name);
        }
*/
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	super.onPrepareOptionsMenu(menu);
    	menu.clear();
    	getMenuInflater().inflate(R.menu.settings, menu);
        for (int i=0; i < ble.getDeviceSize(); i++) {
        	int id = ble.getKey(i);
        	String name = ble.getDeviceName(i);
            menu.add(0, id, 0, name);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                ble.clearDevices();
                ble.scanMTLTDevices(2500);
                setProgressBarIndeterminateVisibility(true);
                return true;
            default:
                //Obtain the discovered device to connect with
            	int item_id = item.getItemId();
            	String name = ble.getDeviceNameByKey(item_id);
                Log.i(TAG, "Connecting to " + name);
                /*
                 * Make a connection with the device using the special LE-specific
                 * connectGatt() method, passing in a callback for GATT events
                 */
                int ret = ble.connectDevice(item.getItemId());
                if (ret != -1)
                {
	       			//TextView tv = (TextView)findViewById(R.id.textView1);
	       			//BluetoothDevice device = mDevices.valueAt(0);
                	DeviceConn.setText(name);
                	Toast.makeText(this, "Connected...", Toast.LENGTH_SHORT).show();
                }
                else {
            		Toast.makeText(this, "Failed to Connect...", Toast.LENGTH_SHORT).show();
            		return true;
                }

                //Display progress UI
                //mHandler.sendMessage(Message.obtain(null, MSG_PROGRESS, "Connecting to "+device.getName()+"..."));
                return super.onOptionsItemSelected(item);
        }
    }



    
    public void loopTest(BluetoothGatt gatt)
    {
    	List<BluetoothGattService> gattServices = gatt.getServices();
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = "unknown service";
        String unknownCharaString = "unknown characteristic";

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            uuid = gattService.getUuid().toString();
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();

           // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic :
                    gattCharacteristics) {

                uuid = gattCharacteristic.getUuid().toString();
            }

         }
    }
    
    public int getNumServices(BluetoothGatt gatt)
    {
    	List<BluetoothGattService> gattServices = gatt.getServices();
    	return gattServices.size(); 
    }
    
    public void onECDH(View v)
    {
    	if (!ble.IsConnected())
    	{
    		mHandler.sendEmptyMessage(3);
    		return;
    	}
    	
    	Bundle params = new Bundle();
        byte[] val = "this is the public key of mobile".getBytes();
        params.putByteArray(Utils.STR_KEY, val);
        
        params.putByte(Utils.STR_COMMAND, Utils.ENUM_CMD_SEND_PUBLIC_KEY);
        
        BleUtils ble_task = new BleUtils();
        
        ble_task.init(this, mHandler, this);
        
        ble_task.execute(params);

        //ble.SendPublicKey(params);
    }
    
    public void onHandshake(View v)
    {
    	if (!ble.IsConnected())
    	{
    		mHandler.sendEmptyMessage(3);
    		return;
    	}
    	
    	Bundle params = new Bundle();

        params.putByteArray(Utils.STR_APP_ID, appId);
        params.putByteArray(Utils.STR_RANDOM_KEY, ble.manipulateRandom(lastRandom));
        
        params.putByte(Utils.STR_COMMAND, Utils.ENUM_CMD_HANDSHAKE1);
        
        BleUtils ble_task = new BleUtils();
        
        ble_task.init(this, mHandler, this);
        
        ble_task.execute(params);

        //ble.handShake(params);
    }
    
    public void onSetOwner(View v)
    {
    	if (!ble.IsConnected())
    	{
    		mHandler.sendEmptyMessage(3);
    		return;
    	}
    	
    	Bundle params = new Bundle();

        params.putByteArray(Utils.STR_PREV_ADMIN_CODE, prevCode);
        params.putByteArray(Utils.STR_ADMIN_CODE, newCode);
        params.putByteArray(Utils.STR_APP_ID, appId);
        params.putByteArray(Utils.STR_USER_ID, userId);
        params.putByte(Utils.STR_MODE, (byte)0);
        params.putByteArray(Utils.STR_RANDOM_KEY, ble.manipulateRandom(lastRandom));
        params.putByteArray(Utils.STR_LOCK_NAME, lockName);
        params.putByteArray(Utils.STR_KEY_NAME, ownerKeyName);
        
        params.putByte(Utils.STR_COMMAND, Utils.ENUM_CMD_SET_OWNER);
        
        BleUtils ble_task = new BleUtils();
        
        ble_task.init(this, mHandler, this);
        
        ble_task.execute(params);

        //ble.setOwner(params);
    }
    
    public void onKDF(View v)
    {
    	if (!ble.IsConnected())
    	{
    		mHandler.sendEmptyMessage(3);
    		return;
    	}
    	Bundle params = new Bundle();

        params.putByte(Utils.STR_BLE_ASSOCIATION_ID, handShakeId);
        
        params.putByte(Utils.STR_COMMAND, Utils.ENUM_CMD_KDF);
        
        BleUtils ble_task = new BleUtils();
        
        ble_task.init(this, mHandler, this);
        
        ble_task.execute(params);

        //ble.agreeOnKey(params);
    }
    
    public void onCreateKey(View v)
    {
    	if (!ble.IsConnected())
    	{
    		mHandler.sendEmptyMessage(3);
    		return;
    	}
    	
    	Bundle params = new Bundle();

        params.putByteArray(Utils.STR_ADMIN_CODE, newCode);
        params.putByteArray(Utils.STR_USER_ID, newUserId);
        params.putByteArray(Utils.STR_NEW_USER_PIN, newUserPIN);
        params.putByteArray(Utils.STR_KEY_NAME, userKeyName);
        params.putByteArray(Utils.STR_RANDOM_KEY, ble.manipulateRandom(lastRandom));
        
        params.putByte(Utils.STR_COMMAND, Utils.ENUM_CMD_CREATE_NEW_KEY);
        
        BleUtils ble_task = new BleUtils();
        
        ble_task.init(this, mHandler, this);
        
        ble_task.execute(params);

        //ble.createNewKey(params);
    }
    
    public void onGetKey(View v)
    {
    	if (!ble.IsConnected())
    	{
    		mHandler.sendEmptyMessage(3);
    		return;
    	}
    	
    	Bundle params = new Bundle();

        params.putByteArray(Utils.STR_USER_ID, newUserId);
        params.putByteArray(Utils.STR_NEW_USER_PIN, newUserPIN);
        params.putByteArray(Utils.STR_APP_ID, appId);
        params.putByteArray(Utils.STR_KEY_NAME, userKeyName);
        params.putByteArray(Utils.STR_RANDOM_KEY, ble.manipulateRandom(lastRandom));
        
        params.putByte(Utils.STR_COMMAND, Utils.ENUM_CMD_GET_NEW_KEY);
        
        BleUtils ble_task = new BleUtils();
        
        ble_task.init(this, mHandler, this);
        
        ble_task.execute(params);

        //ble.getNewKey(params);
    }
    
    public void onUnlock(View v)
    {
    	if (!ble.IsConnected())
    	{
    		mHandler.sendEmptyMessage(3);
    		return;
    	}
    	
    	Bundle params = new Bundle();

        params.putByteArray(Utils.STR_USER_ID, userId);
        params.putByteArray(Utils.STR_APP_ID, appId);
        params.putByteArray(Utils.STR_KEY, eKey);
        params.putByte(Utils.STR_MODE, (byte)0);
        params.putByteArray(Utils.STR_RANDOM_KEY, ble.manipulateRandom(lastRandom));
        
        params.putByte(Utils.STR_COMMAND, Utils.ENUM_CMD_UNLOCK);
        
        BleUtils ble_task = new BleUtils();
        
        ble_task.init(this, mHandler, this);
        
        ble_task.execute(params);

        //ble.unlock(params);
    }
    
    public void onLock(View v)
    {
    	if (!ble.IsConnected())
    	{
    		mHandler.sendEmptyMessage(3);
    		return;
    	}
    	
    	Bundle params = new Bundle();

        params.putByteArray(Utils.STR_USER_ID, newUserId);
        params.putByteArray(Utils.STR_APP_ID, appId);
        params.putByteArray(Utils.STR_KEY, eUserKey);
        params.putByte(Utils.STR_MODE, (byte)0);
        params.putByteArray(Utils.STR_RANDOM_KEY, ble.manipulateRandom(lastRandom));
        
        params.putByte(Utils.STR_COMMAND, Utils.ENUM_CMD_LOCK);
        
        BleUtils ble_task = new BleUtils();
        
        ble_task.init(this, mHandler, this);
        
        ble_task.execute(params);

        //ble.lock(params);
    }
    
    public void onSecPage(View v)
    {
		Intent intent = new Intent(this, SecPage.class);
		intent.setAction(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		return;
    }
    
    //perf test params
    long time_before, time_after;
    private boolean perfOn = false;
    
    private Runnable mPerfRunnable = new Runnable() {
        @Override
        public void run() {
        	Bundle params = new Bundle();
            byte[] val = "this is the public key of mobile".getBytes();
            params.putByteArray(Utils.STR_KEY, val);
            int dummy = 0;
            while (dummy++ < 1)
            {
            	//time_before = System.currentTimeMillis();
            	time_before = System.nanoTime();
            	ble.SendPublicKey(params);
            	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
    };
    
    public void onPerf(View v)
    {
    	//Button button = (Button)findViewById(R.id.button9);
    	//if (!perfOn)
    	//{
    		//button.setText("Perf - Stop");
    		//run onECDH until stopped
    		mHandler.postDelayed(mPerfRunnable, 0);
    	//} else {
    		//button.setText("Perf - Start");
    	//}
    	//perfOn = !perfOn;

		return;
    }

	@Override
	public void onSendPublicKeyResponse(Bundle response) {
		Message msg = new Message();
		String str = null;
		Bundle bundle = new Bundle();
		BlePublicKey = response.getByteArray(Utils.STR_PUBLIC_KEY);
		lastRandom = response.getByteArray(Utils.STR_RANDOM_KEY);
		msg.what = 100;
		try {
			str = new String(BlePublicKey, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		bundle.putString("HANDLER_MESSAGE", str);
		msg.setData(bundle);
		mHandler.sendMessage(msg);
	}

	@Override
	public void onLeScanDeviceFound() {
        //Update the overflow menu
        invalidateOptionsMenu();
		
	}

	@Override
	public void onHandshakeResponse(Bundle response) {
		lastRandom = response.getByteArray(Utils.STR_RANDOM_KEY);
		handShakeId = response.getByte(Utils.STR_BLE_ASSOCIATION_ID);
		byte[] signature = response.getByteArray(Utils.STR_BLE_SIGNATURE);
		//need to verify signature here!!!
		mHandler.sendEmptyMessage(6);
		
	}

	@Override
	public void onSetOwnerResponse(Bundle response) {
		lastRandom = response.getByteArray(Utils.STR_RANDOM_KEY);
		eKey = response.getByteArray(Utils.STR_BLE_EKEY);
		mHandler.sendEmptyMessage(6);
	}

	@Override
	public void onLastRequestSuccess() {
		mHandler.sendEmptyMessage(6);
	}

	@Override
	public void onRequestSuccess(Bundle response) {
		// TODO Auto-generated method stub
		int cmd = response.getInt(Utils.STR_COMMAND);
		switch (cmd) {
		case Utils.ENUM_CMD_GET_NEW_KEY_RESPONSE:
		case Utils.ENUM_CMD_UNLOCK_RESPONSE:
		case Utils.ENUM_CMD_LOCK_RESPONSE:
		case Utils.ENUM_CMD_CREATE_NEW_KEY_RESPONSE:
			mHandler.sendEmptyMessage(cmd);
			break;
		default:
			break;
		}
	}

	@Override
	public void onRequestError(Bundle response) {
		// TODO Auto-generated method stub
		if (response == null)
		{
			mHandler.sendEmptyMessage(9);
			return;
		}
		Message msg = new Message();
		String str = null;
		Bundle bundle = new Bundle();
		byte[] errorType = response.getByteArray(Utils.STR_ERROR_CODE_TYPE);
		byte[] errorDetails = response.getByteArray(Utils.STR_ERROR_CODE_DETAIL);
		msg.what = 100;
		switch ((int)((int)errorDetails[1] << 8) | ((int)errorDetails[0] & 0xFF))
		{
			case 7:
				str = "illegal owner code";
				break;
			case 13:
				str = "wrong user ID";
				break;
			case 14:
				str = "wrong user PIN";
				break;
			case 17:
				str = "wrong eKey";
				break;
			default:
				str = "undefined error";
				break;
		}

		bundle.putString("HANDLER_MESSAGE", str);
		msg.setData(bundle);
		mHandler.sendMessage(msg);
	}

	@Override
	public void onStopScan() {
		// TODO Auto-generated method stub
		setProgressBarIndeterminateVisibility(false);
	}

	@Override
	public void onGattConnected() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(2);
	}

	@Override
	public void onGattDisconnected() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(3);
	}

	@Override
	public void onDiscoverServiceFailed() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(8);
	}

	@Override
	public void onResponseControlRead() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(1);
	}

	@Override
	public void onResponsePrimaryRead() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(1);
	}

	@Override
	public void onGattError() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(5);
	}

	@Override
	public void onRequestControlWrite() {
		// TODO Auto-generated method stub
		//perf();
	}
	
	private void perf()
	{
		return;
		/*
		//time_after = System.currentTimeMillis();
		time_after = System.nanoTime();
		long dif = time_after - time_before;
		Bundle bundle = new Bundle();
		Message msg = new Message();
		msg.what = 100;
		bundle.putString("HANDLER_MESSAGE", "time (nano) - " + dif);
		msg.setData(bundle);
		mHandler.sendMessage(msg);
		*/
	}

	@Override
	public void onResponseControlChanged() {
		// TODO Auto-generated method stub
		perf();
		//mHandler.sendEmptyMessage(1);
	}

	@Override
	public void onResponsePrimaryChanged() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(1);
	}

	@Override
	public void onResponseSecondaryChanged() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(0);
	}

	@Override
	public void onSendError() {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(7);
	}

	@Override
	public void onGetNewKeyResponse(Bundle response) {
		// TODO Auto-generated method stub
		eUserKey = response.getByteArray(Utils.STR_BLE_EKEY);
		lastRandom = response.getByteArray(Utils.STR_RANDOM_KEY);
		mHandler.sendEmptyMessage(6);
	}

	@Override
	public void onKdfResponse(Bundle response) {
		// TODO Auto-generated method stub
		mHandler.sendEmptyMessage(6);
	}


	@Override
	public boolean isDeviceApproved(String name, UUID uuid) {
		// TODO Auto-generated method stub
		return true;
		//return name.equals("SBX-MTLT");
	}

}
